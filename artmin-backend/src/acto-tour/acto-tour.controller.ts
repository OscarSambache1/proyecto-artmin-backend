import { BadRequestException, Body, Controller, InternalServerErrorException, Post } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {ActoTourEntity} from './acto-tour.entity';
import { ActoTourService, CrearActosTourInterface } from './acto-tour.service';

@Controller('acto-tour')
export class ActoTourController extends PrincipalAbstractController<ActoTourEntity> {
    constructor(private readonly _actoTourService: ActoTourService,
    ) {
        super(_actoTourService);
    }

    @Post('crear-actos-tour')
    async crearActosTour(
      @Body() datos: CrearActosTourInterface,
    ) {
        try {
            return this._actoTourService.crearActosTour(
              datos,
            );
        } catch (e) {
            console.error(e);
            throw new InternalServerErrorException({
                mensaje:
                  e.response?.mensaje ||
                  'Error al crear actos tour',
                error: e.response?.error,
            });
        }
    }
}
