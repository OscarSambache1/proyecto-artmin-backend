import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { PrincipalService } from '../principal/principal-service';
import { ActoTourEntity } from './acto-tour.entity';
import { CancionSetlistEntity } from '../cancion-setlist/cancion-setlist.entity';
import { CancionEntity } from '../cancion/cancion.entity';

@Injectable()
export class ActoTourService extends PrincipalService<ActoTourEntity> {
  constructor(
    @InjectRepository(ActoTourEntity)
    private readonly _actoTourRepository: Repository<ActoTourEntity>,
  ) {
    super(_actoTourRepository, ActoTourEntity);

  }

  async crearActosTour(
    datosCrarActos: CrearActosTourInterface,
  ) {
    return await getConnection('default').transaction(
      'READ COMMITTED',
      async (entityManager) => {
        const repositorioCancionSetlist = entityManager.getRepository(CancionSetlistEntity);
        const repositorioActoTour = entityManager.getRepository(ActoTourEntity);
        const cancionesActoSetlist = await repositorioCancionSetlist
          .createQueryBuilder('cancionSetlist')
          .innerJoin('cancionSetlist.actoTour', 'actoTour')
          .innerJoin('actoTour.setlist', 'setlist', `setlist.id = ${datosCrarActos.idSetlist}`)
          .getMany();

        if (cancionesActoSetlist.length) {
          const idsCancionesAEliminar = cancionesActoSetlist.map(cancion => cancion.id);
          await await repositorioCancionSetlist.delete(idsCancionesAEliminar);
        }
        await repositorioActoTour.delete(
          {
            setlist: datosCrarActos.idSetlist,
          },
        );

        let i = 1;
        for await (const actoTour of datosCrarActos.actosTour) {
          const actoTourACrear: Partial<ActoTourEntity> = {
            setlist: datosCrarActos.idSetlist,
            nombre: actoTour.nombre,
            posicion: i,
          };
          const actoTourCreado = await repositorioActoTour.save({ ...actoTourACrear });
          let j = 1;
          if (actoTour.cancionesSetlistTour?.length) {
            for await (const cancionActo of actoTour.cancionesSetlistTour) {
              const cancionSetlistACrear: Partial<CancionSetlistEntity> = {
                cancion: (cancionActo.cancion as CancionEntity).id,
                nombre: cancionActo.nombre,
                actoTour: actoTourCreado.id,
                posicion: j,
              };
              await repositorioCancionSetlist.save(cancionSetlistACrear);
              j++;
            }
            i++;
          }
        }

        return {
          mensaje: 'ok',
        };
      });
  }
}

export interface CrearActosTourInterface {
  idTour: number;
  idSetlist: number;
  actosTour: ActoTourEntity[];
}
