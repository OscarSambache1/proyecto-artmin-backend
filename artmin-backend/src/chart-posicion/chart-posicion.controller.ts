import { BadRequestException, Body, Controller, Get, Post, Query } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {ChartPosicionEntity} from './chart-posicion.entity';
import {ChartPosicionService} from './chart-posicion.service';

@Controller('chart-posicion')
export class ChartPosicionController extends PrincipalAbstractController<ChartPosicionEntity> {
    constructor(private readonly _chartPosicionService: ChartPosicionService,
    ) {
        super(_chartPosicionService);
    }

    @Get('obtener-charts-posiciones')
    async obtenerChartsPosiciones(
      @Query('datosConsulta') datosConsultaCharts) {
        const existeDatos = datosConsultaCharts;
        if (existeDatos) {
            return await this._chartPosicionService
              .obtenerChartsPosiciones(
                JSON.parse(datosConsultaCharts),
              );
        } else {
            throw new BadRequestException('No envía parámetros');
        }
    }
}
