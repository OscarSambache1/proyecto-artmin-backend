import {Column, Entity, ManyToOne} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import {ChartAlbumCancionArtistaVideoEntity} from '../chart-album-cancion-artista-video/chart-album-cancion-artista-video.entity';
import { CancionEntity } from '../cancion/cancion.entity';
import { AlbumEntity } from '../album/album.entity';
import { VideoEntity } from '../video/video.entity';
import { ChartEntity } from '../chart/chart.entity';
import { ArtistaEntity } from '../artista/artista.entity';

@Entity('chart-posicion')
export class ChartPosicionEntity extends PrincipalEntity {
    @Column({
        type: 'int',
        name: 'posicion',
    })
    posicion: number;

    @Column({
        type: 'date',
        name: 'fecha',
    })
    fecha: string;

    @Column({
        type: 'longtext',
        name: 'competencia',
    })
    competencia: string;

    @Column({
        type: 'longtext',
        name: 'observaciones',
        nullable: true,
    })
    observaciones: string;

    @ManyToOne(
      type => ArtistaEntity,
      artista => artista.chartPosicionesArtista,
    )
    artista: ArtistaEntity | number;

    @ManyToOne(
      type => CancionEntity,
      cancion => cancion.chartPosicionesCancion,
    )
    cancion: CancionEntity | number;

    @ManyToOne(
      type => AlbumEntity,
      album => album.chartPosicionesAlbum,
    )
    album: AlbumEntity | number;

    @ManyToOne(
      type => VideoEntity,
      video => video.chartPosicionesVideo,
    )
    video: VideoEntity | number;

    @ManyToOne(
      type => ChartEntity,
      chart => chart.chartPosicionesChart,
    )
    chart: ChartEntity | number;
    //
    // @ManyToOne(
    //     type => ChartAlbumCancionArtistaVideoEntity,
    //     chartAlbumCancionArtistaVideo => chartAlbumCancionArtistaVideo.posicionesChartAlbumCAncionArtistaVideo,
    // )
    // chartAlbumCancionArtistaVideo: ChartAlbumCancionArtistaVideoEntity | number;
}
