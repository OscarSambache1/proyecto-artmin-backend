import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, getRepository } from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import {ChartPosicionEntity} from './chart-posicion.entity';

@Injectable()
export class ChartPosicionService extends PrincipalService<ChartPosicionEntity> {
    constructor(
        @InjectRepository(ChartPosicionEntity)
        private readonly _chartPosicionRepository: Repository<ChartPosicionEntity>,
    ) {
        super(_chartPosicionRepository, ChartPosicionEntity);
    }

    async obtenerChartsPosiciones(
      datosConsultaChartPosicion,
    ) {
        if (datosConsultaChartPosicion.idObjParam) {
            delete datosConsultaChartPosicion.idArtista;
        }
        let condicionNombre = '';
        const condicionArtista = datosConsultaChartPosicion
          .idArtista && datosConsultaChartPosicion.idArtista !== '' ? ' ( artista.id = :idArtista )' : '';
        let condicionObjeto = '';

        if (datosConsultaChartPosicion.tipoObjeto === 'AL') {
            condicionObjeto = datosConsultaChartPosicion.
              idObjParam && datosConsultaChartPosicion.idObjParam !== '' ? ' ( album.id = :idObjParam )' : '';
        }
        if (datosConsultaChartPosicion.tipoObjeto === 'C') {
            condicionObjeto = datosConsultaChartPosicion
              .idObjParam && datosConsultaChartPosicion.idObjParam !== '' ? ' ( cancion.id = :idObjParam )' : '';
        }
        if (datosConsultaChartPosicion.tipoObjeto === 'V') {
            condicionObjeto = datosConsultaChartPosicion
              .idObjParam && datosConsultaChartPosicion.idObjParam !== '' ? ' ( video.id = :idObjParam )' : '';
        }
        const condicionAChart = datosConsultaChartPosicion.idChart !== '' ? ' ( chart.id = :idChart )' : '';

        const consultaBase = await getRepository(ChartPosicionEntity)
          .createQueryBuilder('chartPosicion')
          .leftJoinAndSelect('chartPosicion.chart', 'chart');
        if (datosConsultaChartPosicion.tipo === 'cancion') {
            condicionNombre = datosConsultaChartPosicion.busqueda !== '' ? ' ( cancion.nombre LIKE :busqueda )' : '';
            consultaBase
              .leftJoinAndSelect('chartPosicion.cancion', 'cancion')
              .leftJoinAndSelect('cancion.imagenesCancion', 'imagenCancion')
              .leftJoinAndSelect('cancion.artistasCancion', 'cancionArtista')
              .leftJoinAndSelect('cancionArtista.artista', 'artista');
        }
        if (datosConsultaChartPosicion.tipo === 'album') {
            condicionNombre = datosConsultaChartPosicion.busqueda !== '' ? ' ( album.nombre LIKE :busqueda )' : '';
            consultaBase
              .leftJoinAndSelect('chartPosicion.album', 'album')
              .leftJoinAndSelect('album.imagenesAlbum', 'imagenAlbum')
              .leftJoinAndSelect('album.artistasAlbum', 'albumArtista')
              .leftJoinAndSelect('albumArtista.artista', 'artista');
        }
        if (datosConsultaChartPosicion.tipo === 'video') {
            condicionNombre = datosConsultaChartPosicion.busqueda !== '' ? ' ( video.nombre LIKE :busqueda )' : '';
            consultaBase
              .leftJoinAndSelect('chartPosicion.video', 'video')
              .leftJoinAndSelect('video.cancion', 'cancion')
              .leftJoinAndSelect('cancion.artistasCancion', 'cancionArtista')
              .leftJoinAndSelect('cancionArtista.artista', 'artista');
        }
        const arregloCondiciones = [
            condicionArtista,
            condicionObjeto,
            condicionAChart,
            condicionNombre,
        ].filter(condicionF => condicionF !== '');
        const condicion = arregloCondiciones.reduce((cadena, condicionR, indice) => {
            return cadena + ' ' + condicionR + (indice + 1 === arregloCondiciones.length ? '' : ' AND ');
        }, '');
        return consultaBase
          .where(condicion, {
              busqueda: `%${datosConsultaChartPosicion.busqueda}%`,
              idArtista: datosConsultaChartPosicion.idArtista,
              idChart: datosConsultaChartPosicion.idChart,
              idObjParam: datosConsultaChartPosicion.idObjParam,
          })
          .orderBy('chartPosicion.posicion', 'ASC')
          .getManyAndCount();
    }
}
