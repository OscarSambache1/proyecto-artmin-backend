import {Body, Controller, Post} from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import { FestivalAnioService } from './festival-anio.service';
import { FestivalAnioEntity } from './festival-anio.entity';

@Controller('festival-anio')
export class FestivalAnioController extends PrincipalAbstractController<FestivalAnioEntity> {
    constructor(private readonly _festivalAnioService: FestivalAnioService,
    ) {
        super(_festivalAnioService);
    }
}
