import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import { FestivalLugarEntity } from '../festival-lugar/festival-lugar.entity';

@Entity('festival-anio')
export class FestivalAnioEntity extends PrincipalEntity {
  @ManyToOne(
    type => FestivalLugarEntity,
    festivalLugar => festivalLugar.festivalesAnio,
  )
  festivalLugar: FestivalLugarEntity | number;
}
