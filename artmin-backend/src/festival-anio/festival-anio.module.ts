import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import { FestivalAnioController } from './festival-anio.controller';
import { FestivalAnioService } from './festival-anio.service';
import { FestivalAnioEntity } from './festival-anio.entity';

@Module({
    imports: [TypeOrmModule.forFeature([FestivalAnioEntity], 'default')],
    controllers: [FestivalAnioController],
    providers: [FestivalAnioService],
    exports: [FestivalAnioService],
})
export class FestivalAnioModule {}
