import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository, FindOneOptions, Like} from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import { FestivalAnioEntity } from './festival-anio.entity';

@Injectable()
export class FestivalAnioService extends PrincipalService<FestivalAnioEntity> {
    constructor(
        @InjectRepository(FestivalAnioEntity)
        private readonly _festivalAnioRepository: Repository<FestivalAnioEntity>,
    ) {
        super(_festivalAnioRepository, FestivalAnioEntity);
    }
}
