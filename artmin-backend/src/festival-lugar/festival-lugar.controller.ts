import {Body, Controller, Post} from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import { FestivalLugarService } from './festival-lugar.service';
import { FestivalLugarEntity } from './festival-lugar.entity';

@Controller('festival-lugar')
export class FestivalLugarController extends PrincipalAbstractController<FestivalLugarEntity> {
    constructor(private readonly _festivalLugarService: FestivalLugarService,
    ) {
        super(_festivalLugarService);
    }
}
