import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import { LugarEntity } from '../lugar/lugar.entity';
import { FestivalAnioEntity } from '../festival-anio/festival-anio.entity';
import { FestivalEntity } from '../festival/festival.entity';

@Entity('festival-lugar')
export class FestivalLugarEntity extends PrincipalEntity {
    @ManyToOne(
      type => LugarEntity,
      lugar => lugar.festivalLugares,
    )
    lugar: LugarEntity | number;

    @ManyToOne(
      type => FestivalEntity,
      festival => festival.festivalLugares,
    )
    festival: FestivalEntity | number;

    @OneToMany(
      type => FestivalAnioEntity,
      festivalAnio => festivalAnio.festivalLugar,
    )
    festivalesAnio: FestivalAnioEntity[];
}
