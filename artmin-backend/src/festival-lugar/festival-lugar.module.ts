import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import { FestivalLugarController } from './festival-lugar.controller';
import { FestivalLugarService } from './festival-lugar.service';
import { FestivalLugarEntity } from './festival-lugar.entity';

@Module({
    imports: [TypeOrmModule.forFeature([FestivalLugarEntity], 'default')],
    controllers: [FestivalLugarController],
    providers: [FestivalLugarService],
    exports: [FestivalLugarService],
})
export class FestivalLugarModule {}
