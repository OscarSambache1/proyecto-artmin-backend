import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository, FindOneOptions, Like} from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import { FestivalLugarEntity } from './festival-lugar.entity';

@Injectable()
export class FestivalLugarService extends PrincipalService<FestivalLugarEntity> {
    constructor(
        @InjectRepository(FestivalLugarEntity)
        private readonly _festivalLugarRepository: Repository<FestivalLugarEntity>,
    ) {
        super(_festivalLugarRepository, FestivalLugarEntity);
    }
}
