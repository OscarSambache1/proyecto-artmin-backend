import { Body, Controller, FileFieldsInterceptor, InternalServerErrorException, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import { FestivalService } from './festival.service';
import { FestivalEntity } from './festival.entity';

@Controller('festival')
export class FestivalController extends PrincipalAbstractController<FestivalEntity> {
    constructor(private readonly _festivalService: FestivalService,
    ) {
        super(_festivalService);
    }

    @Post('crear-editar-festival')
    @UseInterceptors(FileFieldsInterceptor([{ name: 'imagen', maxCount: 1 }]))
    async crearEditarFestival(
      @UploadedFiles()
        archivos: any,
      @Body('datos') datosString: string,
    ) {
        try {
            const datos = JSON.parse(datosString);
            delete datos.imagen;
            const imagen = archivos?.imagen ? archivos?.imagen[0] : null;
            return await this._festivalService.crearEditarFestival(
              datos,
              imagen,
            );
        } catch (error) {
            console.error(error, 'error');
            throw new InternalServerErrorException('error al crear festival ');
        }

    }
}
