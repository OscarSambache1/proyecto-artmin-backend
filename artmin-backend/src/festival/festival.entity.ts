import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import { ImagenEntity } from '../imagen/imagen.entity';
import { FestivalLugarEntity } from '../festival-lugar/festival-lugar.entity';
import { TourEntity } from '../tour/tour.entity';
import { TourLugarEntity } from '../tour-lugar/tour-lugar.entity';

@Entity('festival')
export class FestivalEntity extends PrincipalEntity {
    @Column({
        type: 'varchar',
        name: 'nombre',
    })
    nombre: string;

        @Column({
        type: 'longtext',
        name: 'descripcion',
        nullable: true,
    })
    descripcion: string;

    @OneToMany(
      type => ImagenEntity,
      imagen => imagen.festival,
    )
    imagenesFestival: ImagenEntity[];

    @OneToMany(
      type => FestivalLugarEntity,
      festivalLugar => festivalLugar.festival,
    )
    festivalLugares: FestivalLugarEntity[];

    @OneToMany(
      type => TourEntity,
      tour => tour.festival,
    )
    festivalesTour: TourEntity[];

    @OneToMany(
      type => TourLugarEntity,
      tourLugar => tourLugar.festival,
    )
    festivalesTourLugares: TourLugarEntity[];
}
