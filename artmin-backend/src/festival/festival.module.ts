import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import { FestivalController } from './festival.controller';
import { FestivalService } from './festival.service';
import { FestivalEntity } from './festival.entity';
import { ImagenModule } from '../imagen/imagen.module';

@Module({
    imports: [TypeOrmModule.forFeature([FestivalEntity], 'default'),
        ImagenModule,
    ],
    controllers: [FestivalController],
    providers: [FestivalService],
    exports: [FestivalService],
})
export class FestivalModule {}
