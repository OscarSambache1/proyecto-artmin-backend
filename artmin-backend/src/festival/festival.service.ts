import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like } from 'typeorm';
import { PrincipalService } from '../principal/principal-service';
import { FestivalEntity } from './festival.entity';
import { ImagenService } from '../imagen/imagen.service';

@Injectable()
export class FestivalService extends PrincipalService<FestivalEntity> {
  constructor(
    @InjectRepository(FestivalEntity)
    private readonly _festivalRepository: Repository<FestivalEntity>,
    private readonly _imagenService: ImagenService,
  ) {
    super(_festivalRepository, FestivalEntity);
  }

  async crearEditarFestival(datos: any, imagen: any) {
    try {
      const festivalPrevio = await this._festivalRepository.findOne({
        where: {
          id: datos.id,
        },
        relations: ['imagenesFestival'],
      });
      const festivalACrear: Partial<FestivalEntity> = {
        id: datos.id,
        nombre: datos.nombre,
        descripcion: datos.descripcion,
      };

      const festivalCreado = await this._festivalRepository.save(festivalACrear);
      const imagenEncontrada = festivalPrevio?.imagenesFestival[0];
      await this._imagenService.guardarImagen(
        imagen,
        'festival',
        1,
        festivalCreado.id,
        imagenEncontrada?.url,
        imagenEncontrada?.id,
      );
      return await this._festivalRepository.findOne({
        where: {
          id: festivalCreado.id,
        },
        relations: ['imagenesFestival'],
      });
    } catch (error) {
      console.error(error, 'error');
      throw new InternalServerErrorException('error al crear festival ');
    }
  }

}
