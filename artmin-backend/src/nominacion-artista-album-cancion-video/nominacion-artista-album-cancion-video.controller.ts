import { Body, Controller, Get, InternalServerErrorException, Post, Query } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import { CreaNominacionesInterface, NominacionArtistaAlbumCancionVideoService } from './nominacion-artista-album-cancion-video.service';
import {NominacionArtistaAlbumCancionVideoEntity} from './nominacion-artista-album-cancion-video.entity';

@Controller('nominacion-artista-album-cancion-video')
export class NominacionArtistaAlbumCancionVideoController extends PrincipalAbstractController<NominacionArtistaAlbumCancionVideoEntity> {
    constructor(private readonly _nominacionArtistaAlbumCancionVideoService: NominacionArtistaAlbumCancionVideoService,
    ) {
        super(_nominacionArtistaAlbumCancionVideoService);
    }

    @Get('obtener-nominaciones')
    async obtenerNominaciones(
      @Query('datos') datos: string,
    ) {
        try {
            const datosObj = JSON.parse(datos);
            const idArtista = datosObj.idArtista;
            const idPremio = datosObj.idPremio;
            const idPremioAnio = datosObj.idPremioAnio;
            const idCategoria = datosObj.idCategoria;
            const idCancion = datosObj.idCancion;
            const idTour = datosObj.idTour;
            const idAlbum = datosObj.idAlbum;
            const idVideo = datosObj.idVideo;
            const tipo = datosObj.tipo;
            return this._nominacionArtistaAlbumCancionVideoService
              .obtenerNominaciones(
                idArtista || 0,
                idPremio,
                idPremioAnio,
                idCategoria,
                idCancion,
                idAlbum,
                idTour,
                idVideo,
                tipo,
            );
        } catch (error) {
            console.error({
                mensaje: 'Error obtener nominaciones',
                error,
            });
            throw new InternalServerErrorException('Error obtener nominaciones');
        }
    }
}
