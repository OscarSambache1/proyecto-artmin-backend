import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import {NominacionArtistaAlbumCancionVideoEntity} from './nominacion-artista-album-cancion-video.entity';
import { NominacionEntity } from '../nominacion/nominacion.entity';
import { ArtistaEntity } from '../artista/artista.entity';
import { CancionEntity } from '../cancion/cancion.entity';

@Injectable()
export class NominacionArtistaAlbumCancionVideoService extends PrincipalService<NominacionArtistaAlbumCancionVideoEntity> {
    constructor(
        @InjectRepository(NominacionArtistaAlbumCancionVideoEntity)
        private readonly _nominacionArtistaAlbumCancionVideoRepository: Repository<NominacionArtistaAlbumCancionVideoEntity>,
    ) {
        super(_nominacionArtistaAlbumCancionVideoRepository, NominacionArtistaAlbumCancionVideoEntity);
    }

    async obtenerNominaciones(
      idArtista: number,
      idPremio: number,
      idPremioAnio: number,
      idCategoria: number,
      idCancion: number,
      idAlbum: number,
      idTour: number,
      idVideo: number,
      tipo: string,
    ) {
        const query = getRepository(NominacionEntity)
          .createQueryBuilder('nominacion')
          .innerJoinAndSelect('nominacion.categoria', 'categoria')
          .innerJoinAndSelect('nominacion.premioAnio', 'premioAnio')
          .innerJoinAndSelect('premioAnio.premio', 'premio')
          .innerJoinAndSelect('nominacion.nominacionesArtistaAux', 'nominacionArtistaAux')
          .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt');

        if (tipo === 'A') {
            query
              .innerJoinAndSelect('nominacionArtistaAux.artista', 'artista', `artista.id = ${idArtista}`)
              .leftJoinAndSelect('nomArt.cancion', 'cancion')
              .leftJoinAndSelect('cancion.artistasCancion', 'artistasCancion')
              .leftJoinAndSelect('artistasCancion.artista', 'artistaC')
              .leftJoinAndSelect('nomArt.album', 'album')
              .leftJoinAndSelect('album.artistasAlbum', 'artistasAlbum')
              .leftJoinAndSelect('artistasAlbum.artista', 'artistaA')
              .leftJoinAndSelect('nomArt.tour', 'tour')
              .leftJoinAndSelect('tour.artistasTour', 'artistasTour')
              .leftJoinAndSelect('artistasTour.artista', 'artistaT')
              .leftJoinAndSelect('nomArt.video', 'video')
              .leftJoinAndSelect('video.cancion', 'cancionV')
              .leftJoinAndSelect('cancionV.artistasCancion', 'artistasCancionV')
              .leftJoinAndSelect('artistasCancionV.artista', 'artistaV')
              .leftJoinAndSelect('nomArt.artista', 'artistaAux');
        }
        if (tipo === 'C') {
            // traer las nominaciones donde este la cancion con un IN;
            const nominacionRepository = getRepository(NominacionEntity);
            const nominacionesCancion = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.cancion', 'cancion', `cancion.id = ${idCancion}`)
              .select('nominacion.id')
              .getMany();

            const nominacionesVideo = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.video', 'video')
              .innerJoinAndSelect('video.cancion', 'cancionV', `cancionV.id = ${idCancion}`)
              .select('nominacion.id')
              .getMany();

            const idsNominacionCancion = nominacionesCancion.map(nom => nom.id);
            const idsNominacionVideo = nominacionesVideo.map(nom => nom.id);
            const idsNominacion = [...idsNominacionCancion, ...idsNominacionVideo];

            query
              .innerJoinAndSelect('nominacionArtistaAux.artista', 'artista' )
              .leftJoinAndSelect('nomArt.cancion', 'cancion')
              .leftJoinAndSelect('cancion.artistasCancion', 'artistasCancion')
              .leftJoinAndSelect('artistasCancion.artista', 'artistaC')
              .leftJoinAndSelect('nomArt.video', 'video')
              .leftJoinAndSelect('video.cancion', 'cancionV')
              .leftJoinAndSelect('cancionV.artistasCancion', 'artistasCancionV')
              .leftJoinAndSelect('artistasCancionV.artista', 'artistaV')
              .leftJoinAndSelect('nomArt.artista', 'artistaAux')
              .andWhere(`nominacion.id IN (${(idsNominacion.length ? idsNominacion : [0]).join(',')})`);
        }

        if (tipo === 'AL') {
            const nominacionRepository = getRepository(NominacionEntity);
            const nominacionesAlbum = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.album', 'album', `album.id = ${idAlbum}`)
              .select('nominacion.id')
              .getMany();

            const nominacionesCancion = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.cancion', 'cancion')
              .innerJoinAndSelect('cancion.albumesCancion', 'albumesCancion')
              .innerJoinAndSelect('albumesCancion.album', 'album', `album.id = ${idAlbum}`)
              .select('nominacion.id')
              .getMany();

            const idsNominacionCancion = nominacionesCancion.map(nom => nom.id);
            const idsNominacionAlbum = nominacionesAlbum.map(nom => nom.id);
            const idsNominacion = [...idsNominacionCancion, ...idsNominacionAlbum];

            query
              .innerJoinAndSelect('nominacionArtistaAux.artista', 'artista' )
              .leftJoinAndSelect('nomArt.cancion', 'cancion')
              .leftJoinAndSelect('cancion.artistasCancion', 'artistasCancion')
              .leftJoinAndSelect('artistasCancion.artista', 'artistaC')
              .leftJoinAndSelect('nomArt.album', 'album')
              .leftJoinAndSelect('album.artistasAlbum', 'artistasAlbum')
              .leftJoinAndSelect('artistasAlbum.artista', 'artistaA')
              .leftJoinAndSelect('nomArt.artista', 'artistaAux')
              .andWhere(`nominacion.id IN (${(idsNominacion.length ? idsNominacion : [0]).join(',')})`);
        }

        if (tipo === 'T') {
            const nominacionRepository = getRepository(NominacionEntity);
            const nominacionesTour = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.tour', 'tour', `tour.id = ${idTour}`)
              .select('nominacion.id')
              .getMany();

            const idsNominacionTour = nominacionesTour.map(nom => nom.id);
            const idsNominacion = [...idsNominacionTour];

            query
              .innerJoinAndSelect('nominacionArtistaAux.artista', 'artista' )
              .leftJoinAndSelect('nomArt.tour', 'tour')
              .leftJoinAndSelect('tour.artistasTour', 'artistasTour')
              .leftJoinAndSelect('artistasTour.artista', 'artistaT')
              .andWhere(`nominacion.id IN (${(idsNominacion.length ? idsNominacion : [0]).join(',')})`);

        }
        if (tipo === 'V') {
            const nominacionRepository = getRepository(NominacionEntity);
            const nominacionesVideo = await nominacionRepository
              .createQueryBuilder('nominacion')
              .innerJoinAndSelect('nominacion.nominacionesCategoria', 'nomArt')
              .innerJoinAndSelect('nomArt.video', 'video', `video.id = ${idVideo}`)
              .select('nominacion.id')
              .getMany();

            const idsNominacionVideo = nominacionesVideo.map(nom => nom.id);
            const idsNominacion = [...idsNominacionVideo];

            query
              .innerJoinAndSelect('nominacionArtistaAux.artista', 'artista' )
              .leftJoinAndSelect('nomArt.video', 'video')
              .leftJoinAndSelect('video.cancion', 'cancionV')
              .leftJoinAndSelect('cancionV.artistasCancion', 'artistasCancionV')
              .leftJoinAndSelect('artistasCancionV.artista', 'artistaV')
              .andWhere(`nominacion.id IN (${(idsNominacion.length ? idsNominacion : [0]).join(',')})`);

        }

        if (idPremio) {
            query.andWhere(
              `premio.id = ${idPremio}`,
            );
        }

        if (idPremioAnio) {
            query.andWhere(
              `premioAnio.id = ${idPremioAnio}`,
            );
        }

        if (idCategoria) {
            query.andWhere(
              `categoria.id = ${idCategoria}`,
            );
        }

        return await query.getManyAndCount();

    }
}

export interface CreaNominacionesInterface {
    idPremioAnio: number;
    idPremio: number;
    idCategoria: number;
    idNominacion: null;
    tipoCategoria: string;
    nominacionesCategoria: NominacionArtistaAlbumCancionVideoEntity[];
    nominacionACrear: NominacionEntity;
    artistas: ArtistaEntity[];
}
