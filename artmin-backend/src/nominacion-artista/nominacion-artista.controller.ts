import { Body, Controller, InternalServerErrorException, Post } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import { NominacionArtistaEntity } from './nominacion-artista.entity';
import { NominacionArtistaService } from './nominacion-artista.service';

@Controller('nominacion-artista')
export class NominacionArtistaController extends PrincipalAbstractController<NominacionArtistaEntity> {
    constructor(private readonly _nominacionArtistaService: NominacionArtistaService,
    ) {
        super(_nominacionArtistaService);
    }
}
