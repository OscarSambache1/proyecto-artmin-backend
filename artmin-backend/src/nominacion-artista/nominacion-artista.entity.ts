import {Column, Entity, ManyToOne} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import {ArtistaEntity} from '../artista/artista.entity';
import {CancionEntity} from '../cancion/cancion.entity';
import {AlbumEntity} from '../album/album.entity';
import {VideoEntity} from '../video/video.entity';
import { TourEntity } from '../tour/tour.entity';
import { NominacionEntity } from '../nominacion/nominacion.entity';

@Entity('nominacion-artista-aux')
export class NominacionArtistaEntity extends PrincipalEntity {
    @ManyToOne(
        type => ArtistaEntity,
        artista => artista.nominacionesArtistaAux,
    )
    artista: ArtistaEntity | number;

    @ManyToOne(
        type => NominacionEntity,
      nominacion => nominacion.nominacionesArtistaAux,
    )
    nominacion: NominacionEntity | number;
}
