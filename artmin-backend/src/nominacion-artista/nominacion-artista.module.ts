import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {NominacionArtistaController} from './nominacion-artista.controller';
import { NominacionArtistaEntity } from './nominacion-artista.entity';
import { NominacionArtistaService } from './nominacion-artista.service';

@Module({
    imports: [TypeOrmModule.forFeature([NominacionArtistaEntity], 'default')],
    controllers: [NominacionArtistaController],
    providers: [NominacionArtistaService],
    exports: [NominacionArtistaService],
})
export class NominacionArtistaModule {}
