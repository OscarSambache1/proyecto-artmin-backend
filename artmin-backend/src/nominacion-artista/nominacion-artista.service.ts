import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, getConnection } from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import { NominacionArtistaEntity } from './nominacion-artista.entity';

@Injectable()
export class NominacionArtistaService extends PrincipalService<NominacionArtistaEntity> {
    constructor(
        @InjectRepository(NominacionArtistaEntity)
        private readonly _nominacionArtistaRepository: Repository<NominacionArtistaEntity>,
    ) {
        super(_nominacionArtistaRepository, NominacionArtistaEntity);
    }
}
