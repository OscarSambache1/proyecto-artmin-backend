import { Body, Controller, InternalServerErrorException, Post } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {NominacionService} from './nominacion.service';
import {NominacionEntity} from './nominacion.entity';
import { CreaNominacionesInterface } from '../nominacion-artista-album-cancion-video/nominacion-artista-album-cancion-video.service';

@Controller('nominacion')
export class NominacionController extends PrincipalAbstractController<NominacionEntity> {
    constructor(private readonly _nominacion: NominacionService,
    ) {
        super(_nominacion);
    }

    @Post('crear-editar-nominacion')
    async crearEditarNominacion(
      @Body() datos: CreaNominacionesInterface,
    ) {
        try {
            return this._nominacion.crearNominaciones(
              datos,
            );
        } catch (e) {
            console.error(e);
            throw new InternalServerErrorException({
                mensaje:
                  e.response?.mensaje ||
                  'Error al crear nominaciones',
                error: e.response?.error,
            });
        }
    }

    @Post('seleccionar-ganadores')
    async editarGanadores(
      @Body() datos: any,
    ) {
        try {
            return this._nominacion.editarGanadores(
              datos.idNominacionesGana,
              datos.idNominacionesNoGana,
            );
        } catch (e) {
            console.error(e);
            throw new InternalServerErrorException({
                mensaje:
                  e.response?.mensaje ||
                  'Error al seleccionar ganadores',
                error: e.response?.error,
            });
        }
    }
}
