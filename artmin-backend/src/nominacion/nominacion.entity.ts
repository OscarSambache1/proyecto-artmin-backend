import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import {ArtistaEntity} from '../artista/artista.entity';
import {CancionEntity} from '../cancion/cancion.entity';
import {AlbumEntity} from '../album/album.entity';
import {VideoEntity} from '../video/video.entity';
import {PremioAnioEntity} from '../premio-anio/premio-anio.entity';
import {CategoriaEntity} from '../categoria/categoria.entity';
import { TourEntity } from '../tour/tour.entity';
import { NominacionArtistaAlbumCancionVideoEntity } from '../nominacion-artista-album-cancion-video/nominacion-artista-album-cancion-video.entity';
import { NominacionArtistaEntity } from '../nominacion-artista/nominacion-artista.entity';

@Entity('nominacion')
export class NominacionEntity extends PrincipalEntity {
    @ManyToOne(
        type => PremioAnioEntity,
        prmeioAnio => prmeioAnio.nominaciones,
    )
    premioAnio: PremioAnioEntity | number;

    @ManyToOne(
      type => CategoriaEntity,
      categoria => categoria.nominaciones,
    )
    categoria: CategoriaEntity | number;

    @OneToMany(
        type => NominacionArtistaAlbumCancionVideoEntity,
        nominacionCategoria => nominacionCategoria.nominacion,
    )
    nominacionesCategoria: NominacionArtistaAlbumCancionVideoEntity[];

    @OneToMany(
      type => NominacionArtistaEntity,
      nominacionArtista => nominacionArtista.nominacion,
    )
    nominacionesArtistaAux: NominacionArtistaEntity[];
}
