import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {NominacionEntity} from './nominacion.entity';
import {NominacionService} from './nominacion.service';
import {NominacionController} from './nominacion.controller';

@Module({
    imports: [TypeOrmModule.forFeature([NominacionEntity], 'default')],
    controllers: [NominacionController],
    providers: [NominacionService],
    exports: [NominacionService],
})
export class NominacionModule {}
