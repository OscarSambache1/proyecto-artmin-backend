import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, getConnection, In } from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import {NominacionEntity} from './nominacion.entity';
import { NominacionArtistaAlbumCancionVideoEntity } from '../nominacion-artista-album-cancion-video/nominacion-artista-album-cancion-video.entity';
import { CreaNominacionesInterface } from '../nominacion-artista-album-cancion-video/nominacion-artista-album-cancion-video.service';
import { NominacionArtistaEntity } from '../nominacion-artista/nominacion-artista.entity';

@Injectable()
export class NominacionService extends PrincipalService<NominacionEntity> {
    constructor(
        @InjectRepository(NominacionEntity)
        private readonly _nominacionArtistaAlbumCancionVideoRepository: Repository<NominacionEntity>,
    ) {
        super(_nominacionArtistaAlbumCancionVideoRepository, NominacionEntity);
    }

    async crearNominaciones(
      datosCrearNominacion: CreaNominacionesInterface,
    ) {
        return await getConnection('default').transaction(
          'READ COMMITTED',
          async (entityManager) => {
              const repositorioNominacion = entityManager.getRepository(NominacionEntity);
              const repositorioNominacionArtista = entityManager.getRepository(NominacionArtistaAlbumCancionVideoEntity);
              const repositorioNominacionArtistaAux = entityManager.getRepository(NominacionArtistaEntity);

              if (datosCrearNominacion.idNominacion) {
                  await repositorioNominacionArtista.delete(
                    {
                        nominacion: datosCrearNominacion.idNominacion,
                    },
                  );
                  await repositorioNominacionArtistaAux.delete(
                  {
                    nominacion: datosCrearNominacion.idNominacion,
                  },
                );
              }

              if (datosCrearNominacion.idNominacion) {
                  datosCrearNominacion.nominacionACrear.id = datosCrearNominacion.idNominacion;
              }
              const nominacion = await repositorioNominacion.save(datosCrearNominacion.nominacionACrear);

              for await (const nominacionCategoria of datosCrearNominacion.nominacionesCategoria) {
                  const campo = datosCrearNominacion.tipoCategoria.toLowerCase();
                  const nominacionACrear: Partial<NominacionArtistaAlbumCancionVideoEntity> = {
                      siGano: 0,
                      nominacion: nominacion.id,
                      [campo]: nominacionCategoria[campo],
                  };
                  await repositorioNominacionArtista.save({ ...nominacionACrear });
              }

              for await (const artista of datosCrearNominacion.artistas) {
              const nominacionArtistaACrear: Partial<NominacionArtistaEntity> = {
                artista: artista?.id,
                nominacion: nominacion.id,
              };
              await repositorioNominacionArtistaAux.save({ ...nominacionArtistaACrear });
            }
              return {
                  mensaje: 'ok',
              };
          });
    }

  async editarGanadores(
    idNominacionesGana: number[],
    idNominacionesNoGana: number[],
  ) {
    return await getConnection('default').transaction(
      'READ COMMITTED',
      async (entityManager) => {
        const repositorioNominacionArtista = entityManager.getRepository(NominacionArtistaAlbumCancionVideoEntity);

        if (idNominacionesGana.length) {
          await repositorioNominacionArtista.update(
            {
              id: In(idNominacionesGana),
            },
            {
              siGano: 1,
            },
          );
        }
        if (idNominacionesNoGana.length) {

           await repositorioNominacionArtista.update(
             {
               id: In(idNominacionesNoGana),
             },
             {
               siGano: 0,
             },
           );
         }

        return {
          mensaje: 'ok',
        };
      });
  }
}
