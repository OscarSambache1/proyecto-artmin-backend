import {
    Body,
    Controller,
    FileFieldsInterceptor,
    HttpCode,
    InternalServerErrorException,
    Param,
    Post,
    UploadedFiles,
    UseInterceptors,
} from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {PremioEntity} from './premio.entity';
import {PremioService} from './premio.service';

@Controller('premio')
export class PremioController extends PrincipalAbstractController<PremioEntity> {
    constructor(private readonly _premioService: PremioService,
    ) {
        super(_premioService);
    }

    @Post('crear-editar-premio')
    @UseInterceptors(FileFieldsInterceptor([{ name: 'imagen', maxCount: 1 }]))
    async crearEditarProducto(
      @UploadedFiles()
        archivos: any,
      @Body('datos') datosString: string,
    ) {
        try {
            const datos = JSON.parse(datosString);
            delete datos.imagen;
            const imagen = archivos?.imagen ? archivos?.imagen[0] : null;
            return await this._premioService.crearEditarPremio(
              datos,
              imagen,
            );
        } catch (error) {
            console.error(error, 'error');
            throw new InternalServerErrorException('error al crear premio ');
        }

    }

}
