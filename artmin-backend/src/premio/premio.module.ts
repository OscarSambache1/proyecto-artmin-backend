import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PremioController } from './premio.controller';
import { PremioService } from './premio.service';
import { PremioEntity } from './premio.entity';
import { ImagenModule } from '../imagen/imagen.module';
import { CategoriaModule } from '../categoria/categoria.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PremioEntity], 'default'),
    ImagenModule,
    CategoriaModule,
  ],
  controllers: [PremioController],
  providers: [PremioService],
  exports: [PremioService],
})
export class PremioModule {
}
