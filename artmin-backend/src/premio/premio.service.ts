import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PrincipalService } from '../principal/principal-service';
import { PremioEntity } from './premio.entity';
import { ImagenService } from '../imagen/imagen.service';
import { CategoriaService } from '../categoria/categoria.service';

@Injectable()
export class PremioService extends PrincipalService<PremioEntity> {
  constructor(
    @InjectRepository(PremioEntity)
    private readonly _premioRepository: Repository<PremioEntity>,
    private readonly _imagenService: ImagenService,
    private readonly _categoriaService: CategoriaService,
  ) {
    super(_premioRepository, PremioEntity);
  }

  async crearEditarPremio(datos: any, imagen: any) {
    try {
      const premioPrevio = await this._premioRepository.findOne({
        where: {
          id: datos.id,
        },
        relations: ['imagenesPremio', 'categoriasPremio'],
      });
      const premioACrear: Partial<PremioEntity> = {
        id: datos.id,
        nombre: datos.nombre,
        descripcion: datos.descripcion,
        esPremio: datos.esPremio,
      };

      const premioCreado = await this._premioRepository.save(premioACrear);
      const imagenEncontrada = premioPrevio?.imagenesPremio[0];
      await this._imagenService.guardarImagen(
        imagen,
        'premio',
        1,
        premioCreado.id,
        imagenEncontrada?.url,
        imagenEncontrada?.id,
      );
      if (!premioCreado.esPremio) {
        datos.categoriasPremio = [];
      }
      if (datos.categoriasPremio?.length) {
            for await (const categoria of datos.categoriasPremio) {
              if (categoria.id) {
                const categoriaAEditar = {
                  id: categoria.id,
                  nombre: categoria.nombre,
                  descripcion: categoria.descripcion,
                  tipo: categoria.tipo,
                };
                await this._categoriaService.findOneByIdAndUpdate(
                  categoria.id,
                  categoriaAEditar as any,
                );
              } else {
                const categoriaACrear = {
                  premio: premioCreado.id,
                  nombre: categoria.nombre,
                  descripcion: categoria.descripcion,
                  tipo: categoria.tipo,
                };
                await this._categoriaService.create(categoriaACrear as any);
              }
            }
      }
      return await this._premioRepository.findOne({
        where: {
          id: premioCreado.id,
        },
        relations: ['imagenesPremio', 'categoriasPremio'],
      });
    } catch (error) {
      console.error(error, 'error');
      throw new InternalServerErrorException('error al crear premio ');
    }
  }

}
