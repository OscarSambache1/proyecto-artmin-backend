import {Body, Controller, Post} from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {PresentacionCancionEntity} from './presentacion-cancion.entity';
import {PresentacionCancionService} from './presentacion-cancion.service';

@Controller('presentacion-cancion')
export class PresentacionCancionController extends PrincipalAbstractController<PresentacionCancionEntity> {
    constructor(private readonly _presentacionCancionService: PresentacionCancionService,
    ) {
        super(_presentacionCancionService);
    }
}
