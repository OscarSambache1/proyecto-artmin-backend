import {Column, Entity, ManyToOne} from 'typeorm';
import {PrincipalEntity} from '../principal/principal-entity';
import {PresentacionEntity} from '../presentacion/presentacion.entity';
import { CancionEntity } from '../cancion/cancion.entity';

@Entity('presentacion-cancion')
export class PresentacionCancionEntity extends PrincipalEntity {
    @Column({
        type: 'varchar',
        name: 'cancion_nombre',
        nullable: true,
    })
    nombreCancion: string;

    @ManyToOne(
        type => CancionEntity,
        cancion => cancion.presentacionesCancion,
    )
    cancion: CancionEntity | number;

    @ManyToOne(
        type => PresentacionEntity,
        presentacion => presentacion.presentacionesCancion,
    )
    presentacion: PresentacionEntity | number;
}
