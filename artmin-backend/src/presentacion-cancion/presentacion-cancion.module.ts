import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PresentacionCancionEntity } from './presentacion-cancion.entity';
import { PresentacionCancionController } from './presentacion-cancion.controller';
import { PresentacionCancionService } from './presentacion-cancion.service';

@Module({
  imports: [TypeOrmModule.forFeature([PresentacionCancionEntity], 'default')],
  controllers: [PresentacionCancionController],
  providers: [PresentacionCancionService],
  exports: [PresentacionCancionService],
})
export class PresentacionCancionModule {}
