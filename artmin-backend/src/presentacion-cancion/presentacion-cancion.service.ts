import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository, FindOneOptions, Like} from 'typeorm';
import {PrincipalService} from '../principal/principal-service';
import {PresentacionCancionEntity} from './presentacion-cancion.entity';

@Injectable()
export class PresentacionCancionService extends PrincipalService<PresentacionCancionEntity> {
    constructor(
        @InjectRepository(PresentacionCancionEntity)
        private readonly _presentacionCancionRepository: Repository<PresentacionCancionEntity>,
    ) {
        super(_presentacionCancionRepository, PresentacionCancionEntity);
    }
}
