import { Body, Controller, Get, InternalServerErrorException, Post, Query } from '@nestjs/common';
import {PrincipalAbstractController} from '../principal/principal-controller';
import {PresentacionEntity} from './presentacion.entity';
import { CrearPresentacionInterface, PresentacionService } from './presentacion.service';
import { CreaNominacionesInterface } from '../nominacion-artista-album-cancion-video/nominacion-artista-album-cancion-video.service';

@Controller('presentacion')
export class PresentacionController extends PrincipalAbstractController<PresentacionEntity> {
    constructor(private readonly _presentacionService: PresentacionService,
    ) {
        super(_presentacionService);
    }

    @Post('crear-editar-presentacion')
    async crearEditarPresentacion(
      @Body() datos: CrearPresentacionInterface,
    ) {
        try {
            return this._presentacionService.crearPresentacion(
              datos,
            );
        } catch (e) {
            console.error(e);
            throw new InternalServerErrorException({
                mensaje:
                  e.response?.mensaje ||
                  'Error al crear presentacion',
                error: e.response?.error,
            });
        }
    }

    @Get('obtener-presentaciones')
    async obtenerPresentaciones(
      @Query('datos') datos: string,
    ) {
        try {
            const datosObj = JSON.parse(datos);
            const idArtista = datosObj.idArtista;
            const idPremio = datosObj.idPremio;
            const idPremioAnio = datosObj.idPremioAnio;
            const idCancion = datosObj.idCancion;
            const idAlbum = datosObj.idAlbum;
            const tipo = datosObj.tipo;
            return this._presentacionService
              .obtenerPresentacion(
                idArtista || 0,
                idPremio,
                idPremioAnio,
                idCancion,
                idAlbum,
                tipo,
              );
        } catch (error) {
            console.error({
                mensaje: 'Error obtener presentaciones',
                error,
            });
            throw new InternalServerErrorException('Error obtener presentaciones');
        }
    }
}
