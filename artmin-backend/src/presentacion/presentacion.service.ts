import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, getConnection, getRepository } from 'typeorm';
import { PrincipalService } from '../principal/principal-service';
import { PresentacionEntity } from './presentacion.entity';
import { ArtistaEntity } from '../artista/artista.entity';
import { PresentacionCancionEntity } from '../presentacion-cancion/presentacion-cancion.entity';
import { PresentacionArtistaEntity } from '../presentacion-artista/presentacion-artista.entity';
import { CancionEntity } from '../cancion/cancion.entity';

@Injectable()
export class PresentacionService extends PrincipalService<PresentacionEntity> {
  constructor(
    @InjectRepository(PresentacionEntity)
    private readonly _presentacionRepository: Repository<PresentacionEntity>,
  ) {
    super(_presentacionRepository, PresentacionEntity);
  }

  async crearPresentacion(
    datosCrearPresentacion: CrearPresentacionInterface,
  ) {
    return await getConnection('default').transaction(
      'READ COMMITTED',
      async (entityManager) => {
        const repositorioPresentacion = entityManager.getRepository(PresentacionEntity);
        const repositorioPresentacionArtista = entityManager.getRepository(PresentacionArtistaEntity);
        const repositorioPresentacionCancion = entityManager.getRepository(PresentacionCancionEntity);

        if (datosCrearPresentacion.idPresentacion) {
          await repositorioPresentacionArtista.delete(
            {
              presentacion: datosCrearPresentacion.idPresentacion,
            },
          );
          await repositorioPresentacionCancion.delete(
            {
              presentacion: datosCrearPresentacion.idPresentacion,
            },
          );
        }
        let presentacion;
        if (datosCrearPresentacion.idPresentacion) {
          presentacion = await repositorioPresentacion.findOne(datosCrearPresentacion.idPresentacion);
        } else {
          presentacion = await repositorioPresentacion.save(
            {
              descripcion: '',
              premioAnio: datosCrearPresentacion.idPremioAnio,
            },
          );
        }

        for await (const artista of datosCrearPresentacion.artista) {
          await repositorioPresentacionArtista.save(
            {
              presentacion: presentacion.id,
              artista: artista.id,
            });
        }

        for await (const cancion of datosCrearPresentacion.canciones) {
          await repositorioPresentacionCancion.save(
            {
              presentacion: presentacion.id,
              cancion: (cancion.cancion as CancionEntity)?.id,
              nombreCancion: cancion.nombreCancion,
            });
        }
        return {
          mensaje: 'ok',
        };
      });
  }

  async obtenerPresentacion(
    idArtista: number,
    idPremio: number,
    idPremioAnio: number,
    idCancion: number,
    idAlbum: number,
    tipo: string,
  ) {
    const query = getRepository(PresentacionEntity)
      .createQueryBuilder('presentacion')
      .innerJoinAndSelect('presentacion.premioAnio', 'premioAnio')
      .innerJoinAndSelect('premioAnio.premio', 'premio')
      .innerJoinAndSelect('presentacion.presentacionesArtista', 'presentacionArtista')
      .innerJoinAndSelect('presentacion.presentacionesCancion', 'presentacionCancion');

    if (tipo === 'A') {
      query
        .innerJoinAndSelect('presentacionArtista.artista', 'artista', `artista.id = ${idArtista}`)
        .leftJoinAndSelect('presentacionCancion.cancion', 'cancion')
        .leftJoinAndSelect('cancion.artistasCancion', 'artistasCancion')
        .leftJoinAndSelect('artistasCancion.artista', 'artistaC');
    }
    if (tipo === 'C') {
      const presentacionRepository = getRepository(PresentacionEntity);
      const presentacionCancion = await presentacionRepository
        .createQueryBuilder('presentacion')
        .innerJoinAndSelect('presentacion.presentacionesCancion', 'presentacionCancion')
        .innerJoinAndSelect('presentacionCancion.cancion', 'cancion', `cancion.id = ${idCancion}`)
        .select('presentacion.id')
        .getMany();

      const idsPresentacionCancion = presentacionCancion.map(nom => nom.id);

      query
        .leftJoinAndSelect('presentacionArtista.artista', 'artista')
        .leftJoinAndSelect('presentacionCancion.cancion', 'cancion')
        .andWhere(`presentacion.id IN (${(idsPresentacionCancion.length ? idsPresentacionCancion : [0]).join(',')})`);
    }

    if (tipo === 'AL') {
      const presentacionRepository = getRepository(PresentacionEntity);

      const presentacionesCancion = await presentacionRepository
        .createQueryBuilder('presentacion')
        .innerJoinAndSelect('presentacion.presentacionesCancion', 'presentacionCancion')
        .innerJoinAndSelect('presentacionCancion.cancion', 'cancion')
        .innerJoinAndSelect('cancion.albumesCancion', 'albumesCancion')
        .innerJoinAndSelect('albumesCancion.album', 'album', `album.id = ${idAlbum}`)
        .select('presentacion.id')
        .getMany();

      const idsPresentacionCancion = presentacionesCancion.map(nom => nom.id);

      query
        .leftJoinAndSelect('presentacionArtista.artista', 'artista')
        .leftJoinAndSelect('presentacionCancion.cancion', 'cancion')
        .andWhere(`presentacion.id IN (${(idsPresentacionCancion.length ? idsPresentacionCancion : [0]).join(',')})`);
    }

    if (idPremio) {
      query.andWhere(
        `premio.id = ${idPremio}`,
      );
    }

    if (idPremioAnio) {
      query.andWhere(
        `premioAnio.id = ${idPremioAnio}`,
      );
    }
    return await query.getManyAndCount();

  }
}

export interface CrearPresentacionInterface {
  artista: ArtistaEntity[];
  canciones: PresentacionCancionEntity[];
  idPresentacion: number;
  idPremioAnio: number;
}
