import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, Like, getRepository } from 'typeorm';
import { PrincipalService } from '../principal/principal-service';
import { RecordChartEntity } from './record-chart.entity';

@Injectable()
export class RecordChartService extends PrincipalService<RecordChartEntity> {
  constructor(
    @InjectRepository(RecordChartEntity)
    private readonly _recordChartRepository: Repository<RecordChartEntity>,
  ) {
    super(_recordChartRepository, RecordChartEntity);
  }

  async obtenerRecordsChart(
    datosConsultaRecords,
  ) {
    if (datosConsultaRecords.idObjParam) {
      delete datosConsultaRecords.idArtista;
    }
    let condicionNombre = '';
    const condicionArtista = datosConsultaRecords
      .idArtista && datosConsultaRecords.idArtista !== '' ? ' ( artista.id = :idArtista )' : '';
    const condicionAChart = datosConsultaRecords.idChart !== '' ? ' ( chart.id = :idChart )' : '';

    let condicionObjeto = '';

    if (datosConsultaRecords.tipoObjeto === 'AL') {
      condicionObjeto = datosConsultaRecords
        .idObjParam && datosConsultaRecords.idObjParam !== '' ? ' ( album.id = :idObjParam )' : '';
    }
    if (datosConsultaRecords.tipoObjeto === 'C') {
      condicionObjeto = datosConsultaRecords
        .idObjParam && datosConsultaRecords.idObjParam !== '' ? ' ( cancion.id = :idObjParam )' : '';
    }
    if (datosConsultaRecords.tipoObjeto === 'V') {
      condicionObjeto = datosConsultaRecords
        .idObjParam && datosConsultaRecords.idObjParam !== '' ? ' ( video.id = :idObjParam )' : '';
    }
    const condicionACertificado = datosConsultaRecords.idRecord ? ' ( record.id = :idRecord )' : '';

    const consultaBase = await getRepository(RecordChartEntity)
      .createQueryBuilder('recordChart')
      .leftJoinAndSelect('recordChart.chart', 'chart')
      .leftJoinAndSelect('recordChart.medida', 'medida')
      .leftJoinAndSelect('recordChart.record', 'record');

    if (datosConsultaRecords.tipo === 'cancion') {
      condicionNombre = datosConsultaRecords.busqueda !== '' ? ' ( cancion.nombre LIKE :busqueda )' : '';
      consultaBase
        .leftJoinAndSelect('recordChart.cancion', 'cancion')
        .leftJoinAndSelect('cancion.imagenesCancion', 'imagenCancion')
        .leftJoinAndSelect('cancion.artistasCancion', 'cancionArtista')
        .leftJoinAndSelect('cancionArtista.artista', 'artista');
    }
    if (datosConsultaRecords.tipo === 'album') {
      condicionNombre = datosConsultaRecords.busqueda !== '' ? ' ( album.nombre LIKE :busqueda )' : '';
      consultaBase
        .leftJoinAndSelect('recordChart.album', 'album')
        .leftJoinAndSelect('album.imagenesAlbum', 'imagenAlbum')
        .leftJoinAndSelect('album.artistasAlbum', 'albumArtista')
        .leftJoinAndSelect('albumArtista.artista', 'artista');
    }
    if (datosConsultaRecords.tipo === 'video') {
      condicionNombre = datosConsultaRecords.busqueda !== '' ? ' ( video.nombre LIKE :busqueda )' : '';
      consultaBase
        .leftJoinAndSelect('recordChart.video', 'video')
        .leftJoinAndSelect('video.cancion', 'cancion')
        .leftJoinAndSelect('cancion.artistasCancion', 'cancionArtista')
        .leftJoinAndSelect('cancionArtista.artista', 'artista');
    }
    const arregloCondiciones = [
      condicionArtista,
      condicionObjeto,
      condicionAChart,
      condicionACertificado,
      condicionNombre,
    ].filter(condicionF => condicionF !== '');
    const condicion = arregloCondiciones.reduce((cadena, condicionR, indice) => {
      return cadena + ' ' + condicionR + (indice + 1 === arregloCondiciones.length ? '' : ' AND ');
    }, '');
    return consultaBase
      .where(condicion, {
        busqueda: `%${datosConsultaRecords.busqueda}%`,
        idArtista: datosConsultaRecords.idArtista,
        idChart: datosConsultaRecords.idChart,
        idRecord: datosConsultaRecords.idRecord,
        idObjParam: datosConsultaRecords.idObjParam,
      })
      .orderBy('recordChart.fechaRecord', 'DESC')
      .getManyAndCount();
  }
}
